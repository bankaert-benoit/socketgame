package fr.owle.socketgame.server;

import fr.owle.socketgame.client.Client;
import fr.owle.socketgame.client.NewClientFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {

    private ServerSocket socket;
    private List<Client> clients;
    private int numberOfClient;

    public static void main(String[] args) throws IOException {
        new Server(9876).start();
    }

    public Server(int port) throws IOException {
        this.socket = new ServerSocket(port);
        this.clients = new ArrayList<Client>();
        this.numberOfClient = 0;
    }

    public void start() throws IOException {
        Socket client = null;
        while(true){
            client = this.socket.accept();
            final Socket finalClient = client;
            new Thread(){
                @Override
                public void run() {
                    try {
                        System.out.println("New Client !!");
                        clients = NewClientFactory.createNewClientList(finalClient,clients,numberOfClient);
                        numberOfClient++;
                        displayAllClients();
                        BufferedReader in = new BufferedReader(new InputStreamReader(finalClient.getInputStream()));
                        String val = in.readLine();
                        do{
                            System.out.println("Client "+finalClient.toString()+" says : "+val);
                            val = in.readLine();
                        }while(val != null);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }
    }

    public void displayAllClients(){
        System.out.println(this.numberOfClient+" client connected ! (clients list size :"+clients.size()+")");
        for(Client client:this.clients){
            System.out.println(client.getSocket().toString());
        }
    }
}

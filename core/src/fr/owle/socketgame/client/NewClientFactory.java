package fr.owle.socketgame.client;

import java.net.Socket;
import java.util.List;

public class NewClientFactory {

    public static List<Client> createNewClientList(Socket client, List<Client> oldList,int nbClient){
        List<Client> newList = oldList;
        Client c = new Client(client,newList);
        System.out.println("Updating client list !");
        for(int i = 0; i < nbClient; i++){
            System.out.println("Updating client "+(i+1));
            newList.get(i).updateClientList(c);
        }
        System.out.println("Done updating list !");
        System.out.println("Adding new client");
        if(!newList.contains(c)){
            newList.add(nbClient,c);
        }
        return newList;
    }
}

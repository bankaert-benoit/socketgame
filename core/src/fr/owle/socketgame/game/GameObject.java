package fr.owle.socketgame.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class GameObject {

    private int x,y,size;
    private MovmentHandler movmentHandler;

    public GameObject(){
        this.movmentHandler = new MovmentHandler();
        this.x = 0;
        this.y = 0;
        this.size = 50;

    }

    public void render(float delta){
        ShapeRenderer renderer = new ShapeRenderer();
        renderer.begin(ShapeRenderer.ShapeType.Filled);
        renderer.setColor(Color.WHITE);
        renderer.rect(x,y,size,size);
        renderer.end();
        renderer.dispose();
    }

    public MovmentHandler getMovmentHandler() {
        return movmentHandler;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
}

package fr.owle.socketgame.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;

public class MovmentHandler {

    public void move(GameObject player,float width,float height){
        if(Gdx.input.isKeyPressed(Input.Keys.Z)){
            player.setY(constrain(0,(int)height,player.getY()+1));
        }else if(Gdx.input.isKeyPressed(Input.Keys.S)){
            player.setY(constrain(0,(int)height,player.getY()-1));
        }if(Gdx.input.isKeyPressed(Input.Keys.Q)){
            player.setX(constrain(0,(int)width,player.getX()-1));
        }if(Gdx.input.isKeyPressed(Input.Keys.D)){
            player.setX(constrain(0,(int)width,player.getX()+1));
        }
    }

    public int constrain(int min,int max,int value){
        if(value < min) return min;
        else if(value > max) return max;
        else return value;
    }
}

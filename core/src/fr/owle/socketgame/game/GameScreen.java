package fr.owle.socketgame.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import fr.owle.socketgame.SocketGame;
import org.graalvm.compiler.core.common.type.ArithmeticOpTable;

public class GameScreen implements Screen {

    private SocketGame game;
    private OrthographicCamera gamecam;
    private Viewport gameview;
    private GameObject player;

    public GameScreen(SocketGame game){
        this.game = game;
        this.gamecam = new OrthographicCamera(400,400);
        this.gameview = new FitViewport(400,400,gamecam);
        this.player = new GameObject();
    }

    @Override
    public void show() {

    }

    public void update(float delta){
        player.getMovmentHandler().move(player,gamecam.viewportWidth,gamecam.viewportHeight);
    }

    @Override
    public void render(float delta) {
        update(delta);
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        game.batch.begin();
        player.render(delta);
        game.batch.end();
    }

    @Override
    public void resize(int width, int height) {
        gameview.update(width,height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
